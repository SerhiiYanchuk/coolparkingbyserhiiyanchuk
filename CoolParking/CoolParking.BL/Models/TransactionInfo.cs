﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public readonly struct TransactionInfo
    {
        public string TransportId { get; }
        public decimal Sum { get; }
        public DateTime WithdrawTime { get; }
        public TransactionInfo(string transportId, decimal sum)
        {
            TransportId = transportId;
            Sum = sum;
            WithdrawTime = DateTime.Now;
        }
        public override string ToString() => $"Transport ID: {TransportId}  payment: {Sum, -4}  time: {WithdrawTime}";
    }
}