﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using Microsoft.Extensions.Configuration;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    internal static class Settings
    {
        public static decimal StartingParkingBalance { get; } = 0m;
        public static int ParkingCapacity { get; } = 10;
        public static int WithdrawPeriod { get; } = 5; // in seconds
        public static decimal PenaltyCoefficient { get; } = 2.5m;
        public static int LoggingPeriod { get; } = 60; // in seconds

        private static decimal passengerCarTariff = 2m;
        private static decimal truckTariff = 5m;
        private static decimal busTariff = 3.5m;
        private static decimal motorcycleTariff = 1m;
        public static decimal GetTariff(VehicleType type) => type switch
        {
            VehicleType.PassengerCar => passengerCarTariff,
            VehicleType.Truck => truckTariff,
            VehicleType.Bus => busTariff,
            VehicleType.Motorcycle => motorcycleTariff,
            _ => throw new ArgumentException("Undefined vehicle type")
        };
        static Settings()
        {
            //if a config file exists, then override the settings
            FileInfo file = new FileInfo($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\config.json");
            if (file.Exists)
            {
                var builder = new ConfigurationBuilder()
                    .AddJsonFile(file.FullName, optional: false);

                IConfiguration config = builder.Build();

                NumberFormatInfo nfi = new NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";

                string value = config["StartingParkingBalance"];
                if (value is not null)
                    StartingParkingBalance = Convert.ToDecimal(value, nfi);

                value = config["ParkingCapacity"];
                if (value is not null)
                    ParkingCapacity = Convert.ToInt32(value);

                value = config["WithdrawPeriod"];
                if (value is not null)
                    WithdrawPeriod = Convert.ToInt32(value);

                value = config["PenaltyCoefficient"];
                if (value is not null)
                    PenaltyCoefficient = Convert.ToDecimal(value, nfi);

                value = config["LoggingPeriod"];
                if (value is not null)
                    LoggingPeriod = Convert.ToInt32(value, nfi);

                value = config["Tariffs:PassengerCar"];
                if (value is not null)
                    passengerCarTariff = Convert.ToDecimal(value, nfi);

                value = config["Tariffs:Truck"];
                if (value is not null)
                    truckTariff = Convert.ToDecimal(value, nfi);

                value = config["Tariffs:Bus"];
                if (value is not null)
                    busTariff = Convert.ToDecimal(value, nfi);

                value = config["Tariffs:Motorcycle"];
                if (value is not null)
                    motorcycleTariff = Convert.ToDecimal(value, nfi);
            }
        }
    }
}