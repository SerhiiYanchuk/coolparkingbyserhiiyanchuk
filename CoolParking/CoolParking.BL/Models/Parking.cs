﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    internal class Parking
    {
        public decimal Balance { get; set; } = Settings.StartingParkingBalance;
        public List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
        public int ParkingCapacity { get; } = Settings.ParkingCapacity;

        private static Parking instance = null;
        private static readonly object padlock = new object();
        private Parking()
        {
        }
        public static Parking Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance is null)              
                        instance = new Parking();
                    return instance;
                }
            }
        }
    }
}