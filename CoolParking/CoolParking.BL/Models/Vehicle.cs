﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$", RegexOptions.Compiled))
                throw new ArgumentException("Wrong format. Format ID XX-YYYY-XX (where X is any uppercase letter of the English alphabet and Y is any number)", nameof(id));
            if (balance < 0)
                throw new ArgumentException("The balance cannot be initially negative", nameof(balance));
            if (vehicleType == VehicleType.Undefined)
                throw new ArgumentException("The vehicle type cannot be undefined", nameof(vehicleType));

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public override string ToString() => $"ID: {Id}  type: {VehicleType, -12}  balance: {Balance}";

        // Первый вариант генерации ID
        // Полностью рандомный, но есть маленький шанс того, что может сгенерировать ID, который был сгенерирован ранее
        public static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder generatedId = new StringBuilder(10);
            Random rand = new Random();
            generatedId.Append(Convert.ToChar(rand.Next(65, 91))); //UTF-16: A - 65, Z - 90, [-91
            generatedId.Append(Convert.ToChar(rand.Next(65, 91)));
            generatedId.Append('-');
            generatedId.Append(rand.Next(0, 10));
            generatedId.Append(rand.Next(0, 10));
            generatedId.Append(rand.Next(0, 10));
            generatedId.Append(rand.Next(0, 10));
            generatedId.Append('-');
            generatedId.Append(Convert.ToChar(rand.Next(65, 91)));
            generatedId.Append(Convert.ToChar(rand.Next(65, 91)));
            return generatedId.ToString();
        }

        // Второй вариант генерации ID
        // Генерирует ID основываясь на предыдущем сгенерированном ID
        // Получится следующая последовательность: BA-0000-AA, CA-0000-AA, ... , AB-0000-AA, BB-0000-AA, ... , AA-1000-AA, ...

        //private static StringBuilder lastId = new StringBuilder("AA-0000-AA");
        //public static string GenerateRandomRegistrationPlateNumber()
        //{
        //    int[] indexes = new int[] { 0, 1, 3, 4, 5, 6, 8, 9 };
        //    foreach (int i in indexes)
        //    {
        //        if (char.IsLetter(lastId[i]) && lastId[i] == 'Z') // 90
        //        {
        //            lastId[i] = 'A'; // 65
        //            continue;
        //        }
        //        if (char.IsDigit(lastId[i]) && lastId[i] == '9')
        //        {
        //            lastId[i] = '0';
        //            continue;
        //        }
        //        lastId[i] = (char)(lastId[i] + 1);
        //        break;
        //    }
        //    return lastId.ToString();
        //}
    }
}