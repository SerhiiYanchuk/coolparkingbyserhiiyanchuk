﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.

namespace CoolParking.BL.Models
{
    public enum VehicleType : byte
    {
        Undefined = 0,
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}