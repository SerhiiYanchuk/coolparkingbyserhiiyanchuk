﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimeService : ITimerService
    {
        private Timer timer = new Timer();
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }
        public TimeService()
        {
            timer.Elapsed += InvokeEvent;
        }
        private void InvokeEvent(object sender, ElapsedEventArgs e) => Elapsed?.Invoke(sender, e);
        public void Start()
        {
            timer.Interval = Interval;
            timer.Start();
        }
        public void Stop() => timer.Stop();
        public void Dispose() => timer.Dispose();
    }
}