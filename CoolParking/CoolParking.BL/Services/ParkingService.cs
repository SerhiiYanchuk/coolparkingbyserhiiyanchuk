﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking parking = Parking.Instance;
        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;
        private List<TransactionInfo> lastParkingTransactions = new List<TransactionInfo>();
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;

            withdrawTimer.Interval = Settings.WithdrawPeriod * 1000;
            withdrawTimer.Elapsed += WithdrawHandler;
            logTimer.Interval = Settings.LoggingPeriod * 1000;
            logTimer.Elapsed += LoggingHandler;
            withdrawTimer.Start();
            logTimer.Start();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (parking.Vehicles.Count >= parking.ParkingCapacity)
                throw new InvalidOperationException("Parking is full");
            if (IsIdExist(vehicle.Id))
                throw new ArgumentException("Vehicle id exists", nameof(vehicle.Id));
            parking.Vehicles.Add(vehicle);
        }
        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = FindVehicleById(vehicleId);
            if (vehicle is null)
                throw new ArgumentException("Vehicle doesn't exist", nameof(vehicleId));
            if (vehicle.Balance < 0)
                throw new InvalidOperationException($"There is debt (negative balance). Debt: {vehicle.Balance}");
            parking.Vehicles.Remove(vehicle);
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle vehicle = FindVehicleById(vehicleId);
            if (vehicle is null)
                throw new ArgumentException("Vehicle doesn't exist", nameof(vehicleId));
            if (sum < 0)
                throw new ArgumentException("The top-up amount cannot be negative", nameof(sum));
            vehicle.Balance += sum;
        }
        public decimal GetBalance()
        {
            return parking.Balance;
        }
        public int GetCapacity()
        {
            return parking.ParkingCapacity;
        }
        public int GetFreePlaces()
        {
            return parking.ParkingCapacity - parking.Vehicles.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }
        public TransactionInfo[] GetLastParkingTransactions() => lastParkingTransactions.ToArray();
        public string ReadFromLog() => logService.Read();

        private bool IsIdExist(string id) => parking.Vehicles.Any(vehicle => vehicle.Id == id);
        private Vehicle FindVehicleById(string vehicleId) => parking.Vehicles.SingleOrDefault(t => t.Id == vehicleId);
        private void WithdrawHandler(object sender, ElapsedEventArgs e)
        {
            foreach(var vehicle in parking.Vehicles)
            {
                decimal currentSum = vehicle.Balance;
                decimal tariff = Settings.GetTariff(vehicle.VehicleType);
                decimal payment;
                if (currentSum >= tariff)
                    payment = tariff;
                else if (currentSum > 0)
                    payment = currentSum + (tariff - currentSum) * Settings.PenaltyCoefficient;
                else
                    payment = tariff * Settings.PenaltyCoefficient;

                lastParkingTransactions.Add(new TransactionInfo(vehicle.Id, payment)); //время можно брать из ElapsedEventArgs
                parking.Balance += payment;
                vehicle.Balance -= payment;               
            }
        }
        private void LoggingHandler(object sender, ElapsedEventArgs e)
        {
            StringBuilder builder = new StringBuilder();
            // The loop may not have time to go through all the elements before the list is modified, so we call ToArray()
            foreach (var transaction in lastParkingTransactions.ToArray())
            {
                builder.AppendLine(transaction.ToString());
                lastParkingTransactions.Remove(transaction);
            }               
            logService.Write(builder.ToString());
        }
        public void Dispose()
        {
            withdrawTimer.Dispose();
            logTimer.Dispose();
            // append to log file last parking transactions
            this.LoggingHandler(this, null);

            parking.Vehicles.Clear();
            parking.Balance = Settings.StartingParkingBalance;      
        }
    }
}