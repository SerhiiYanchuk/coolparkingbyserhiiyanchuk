﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        public LogService(string _logFilePath)
        {
            LogPath = _logFilePath;
        }
        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("Log file doesn't exist");
            string content = string.Empty;
            try
            {        
                using (StreamReader reader = new StreamReader(LogPath, System.Text.Encoding.Default))
                    content = reader.ReadToEnd();             
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return content;
        }
        public void Write(string logInfo)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(LogPath, true, System.Text.Encoding.Default))
                    writer.WriteLine(logInfo);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }     
    }
}