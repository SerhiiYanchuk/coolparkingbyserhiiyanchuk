﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CoolParking.PL
{
    class Program
    {
        static void Main(string[] args)
        {
            string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            using (ParkingService parking = new ParkingService(new TimeService(), new TimeService(), new LogService(logFilePath)))
            {
                try
                {
                    foreach (var vehicle in VehicleFactory.GenerateVehicles(5))
                        parking.AddVehicle(vehicle);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                while (true)
                {
                    Console.Clear();
                    Console.WriteLine("-----------------------");
                    Console.WriteLine($"{' ',-3}CoolParking\n{' ',-3}Main menu");
                    Console.WriteLine("-----------------------\n");
                    Console.WriteLine("1 - Parking information");
                    Console.WriteLine("2 - Vehicle management");
                    
                    Console.WriteLine("0 - Exit");
                    if (int.TryParse(Console.ReadLine(), out int choice))
                    {
                        switch (choice)
                        {
                            case 1:
                                ParkingInfoSubMenu(parking);
                                break;
                            case 2:
                                VehicleManagementSubMenu(parking);
                                break;
                            case 0:
                                return;
                            default:
                                Console.WriteLine("Incorrect input. Try again. I believe in you :)");
                                break;
                        }
                    }
                }
            }
            
        }
        static void ParkingInfoSubMenu(ParkingService parking)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("----------------------");
                Console.WriteLine($"{' ',-3}CoolParking\n{' ',-3}Parking information");
                Console.WriteLine("----------------------");
                Console.WriteLine("1 - Display the current balance of the Parking");
                Console.WriteLine("2 - Display the amount of money earned for the current period (before logging)");
                Console.WriteLine("3 - Display the number of free / occupied parking spots");
                Console.WriteLine("4 - Display all parking transactions for the current period (before being logged)");
                Console.WriteLine("5 - Display the transaction history (by reading data from the transactions.log file)");
                Console.WriteLine("6 - Display the list of vehicles in the parking");
                Console.WriteLine("0 - Back");
                Console.Write("> ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine($"Current balance: {parking.GetBalance()}");
                            break;
                        case 2:                           
                            Console.WriteLine($"The amount of money earned for the current period: {parking.GetLastParkingTransactions().Sum(tr => tr.Sum)}");
                            break;
                        case 3:
                            Console.WriteLine($"Free spots: {parking.GetFreePlaces()}");
                            Console.WriteLine($"Occupied spots: {parking.GetCapacity() - parking.GetFreePlaces()}");
                            break;
                        case 4:
                            Console.WriteLine("All parking transactions for the current period:");
                            foreach(var transaction in parking.GetLastParkingTransactions())
                                Console.WriteLine(transaction.ToString());
                            break;
                        case 5:
                            try
                            {
                                Console.WriteLine($"The transaction history:\n\n{parking.ReadFromLog()}");
                            }
                            catch(InvalidOperationException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("I will fix it, I promise");
                            }
                            break;
                        case 6:
                            Console.WriteLine("The list of vehicles in the parking:\n");
                            foreach (var vehicle in parking.GetVehicles())
                                Console.WriteLine(" - " + vehicle.ToString());
                            break;
                        case 0:
                            return;
                        default:
                            Console.WriteLine("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                    Console.WriteLine("\nPress any key");
                    Console.ReadKey();
                }
            }
        }
        static void VehicleManagementSubMenu(ParkingService parking)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("-----------------------");
                Console.WriteLine($"{' ',-3}CoolParking\n{' ',-3}Management");
                Console.WriteLine("----------------------");
                Console.WriteLine("1 - Put a vehicle in the parking");
                Console.WriteLine("2 - Pick up a vehicle from the parking");
                Console.WriteLine("3 - Top up the balance of a vehicle");
                Console.WriteLine("0 - Back");
                Console.Write("> ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    string id = string.Empty;
                    switch (choice)
                    {
                        case 1:
                            Vehicle vehicle = VehicleFactory.CreateVehicle();
                            try
                            {
                                if (vehicle is not null)
                                    parking.AddVehicle(vehicle);
                            }
                            catch(ArgumentException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            break;
                        case 2:
                            Console.Write("Transport plate number (YY-XXXX-YY) > ");
                            id = Console.ReadLine();
                            try
                            {
                                parking.RemoveVehicle(id);
                            }
                            catch (InvalidOperationException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            catch(ArgumentException e)
                            {
                                Console.WriteLine(e.Message);
                            }                           
                            break;
                        case 3:
                            Console.Write("Transport plate number (YY-XXXX-YY) > ");
                            id = Console.ReadLine();
                            decimal amount;
                            while (true)
                            {
                                Console.Write("Top-up amount > ");
                                if (decimal.TryParse(Console.ReadLine(), out amount))
                                    break;
                            }
                            try
                            {
                                parking.TopUpVehicle(id, amount);
                            }
                            catch(ArgumentException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            break;
                        case 0:
                            return;
                        default:
                            Console.WriteLine("Incorrect input. Try again. I believe in you :)");
                            break;
                    }
                    Console.WriteLine("\nPress any key");
                    Console.ReadKey();
                }
            }
        }
    }
}
