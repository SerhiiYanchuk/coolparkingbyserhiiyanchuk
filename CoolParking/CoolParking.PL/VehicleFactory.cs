﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.PL
{
    public static class VehicleFactory
    {
        public static Vehicle[] GenerateVehicles(int quantity)
        {
            Vehicle[] vehicles = new Vehicle[quantity];
            for (int i = 0; i < quantity; i++)
            {
                string id = Vehicle.GenerateRandomRegistrationPlateNumber();
                Random random = new Random();
                VehicleType type = (VehicleType)random.Next(1, 5);
                decimal balance = random.Next(0, 201);
                vehicles[i] = new Vehicle(id, type, balance);
            }
            return vehicles;
        }
       
        // input from console
        public static Vehicle CreateVehicle()
        {
            Console.Write("Transport plate number (XX-YYYY-XX) > ");
            string id = Console.ReadLine();

            Console.WriteLine("1 - Passenger car");
            Console.WriteLine("2 - Truck");
            Console.WriteLine("3 - Bus");
            Console.WriteLine("4 - Motorcycle");
            VehicleType type;
            while (true)
            {
                Console.Write("> ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    if (choice <= 4  && choice >= 1)
                    {
                        type = (VehicleType)choice;
                        break;
                    }                     
                    Console.WriteLine("Incorrect input. Try again. I believe in you :)");
                }
            }
            decimal balance;
            while (true)
            {
                Console.Write("Balance > ");
                if (decimal.TryParse(Console.ReadLine(), out balance))
                    break;
            }
            try
            {
                return new Vehicle(id, type, balance);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }    
    }
}
